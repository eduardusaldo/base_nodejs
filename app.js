var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var expressValidator = require('express-validator');
var session = require('express-session')
var flash = require('express-flash');

var app = express();
var env = process.env.NODE_ENV || 'development';
var server = http.createServer(app);
app.use(expressValidator({
  isArray: function(value) {
    return Array.isArray(value);
  },
  gte: function(param, num) {
    return param >= num;
  }
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(flash());

app.use(express.static(path.join(__dirname, 'public')));
app.locals.basedir = __dirname + "/views";

var timeOut = 60*60*1000; // first number define minutes
var SQLiteStore = require('connect-sqlite3')(session);
var sess = {
  secret: "97946f1e58fbf5137467322ee03ee8c5cafe338e02897e98ddf88efad845e7cfdfcbd1d77de2d7a79bd9f7c2ba9971d5d514fcd61b2f7f09533333329eb86cf9",
  cookie: { 
    maxAge: timeOut,
    secure:true
  },
  store: new SQLiteStore({db: env, dir: './db'}),
  resave:false,
  saveUninitialized:false,
}
if(env==="development"){
  sess.cookie.maxAge=null;
  sess.cookie.secure=false;
}
app.use(session(sess));
console.log("Environment : "+env);
console.log("Max age : "+sess.cookie.maxAge);
app.use(function(req, res, next) {
  console.log("Session Expired : "+req.session.cookie._expires)
  next();
});
// call routes.js that manage controllers
require('./routes')(app);




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  console.log("Error Found");
  next(err);
  // res.render('error', {
  //   err:err,
	// });
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

server.setTimeout(timeOut);

module.exports = app;
