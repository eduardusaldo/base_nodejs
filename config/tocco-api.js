'use strict';

var env = process.env.NODE_ENV || 'development';

module.exports = function() {
	var jwtKey = '';
	var initiativeCodeName = 'IB_PRVI';
	var buildNumber = "0.1.0";
	switch(env) {
		case 'production':
		// Base API source for production, using LIVE server
		var uobtpAvalonTransferUrl = 'https://uobtp-avalon-transfer.azurewebsites.net';

		var gtaDestinationsUrl = 'https://tocco-mobility.search.windows.net/indexes/gta-transfer-destinations/docs/search?api-version=2016-09-01';
		
		var gtaDestinationsApiKey = '660D2E85738887AE3A09E52FEE396D27';
		var isFullAmount = false;
		var isLivePayment = false;
		var isBIN_CheckerEnabled = false;
		break;
		
		case 'staging':
		// Base API source for production, using PREPROD server
		var uobtpAvalonTransferUrl = 'https://uobtp-avalon-transfer.azurewebsites.net';

		var gtaDestinationsUrl = 'https://tocco-mobility.search.windows.net/indexes/gta-transfer-destinations/docs/search?api-version=2016-09-01';
		
		var gtaDestinationsApiKey = '660D2E85738887AE3A09E52FEE396D27';
		var isFullAmount = false;
		var isLivePayment = false;
		var isBIN_CheckerEnabled = false;
		break;
		
		// Anything else will be treated as development
		default:
		// Base API source for development, using STAGING server
		var uobtpAvalonTransferUrl = 'https://uobtp-avalon-transfer.azurewebsites.net';

		var gtaDestinationsUrl = 'https://tocco-mobility.search.windows.net/indexes/gta-transfer-destinations/docs/search?api-version=2016-09-01';

		var gtaDestinationsApiKey = '660D2E85738887AE3A09E52FEE396D27';
		var isFullAmount = false;
		var isLivePayment = false;
		var isBIN_CheckerEnabled = false;
	}
	
	return {
		
		uobtpAvalonTransferUrl: uobtpAvalonTransferUrl,
		gtaDestinationsUrl: gtaDestinationsUrl,
		gtaDestinationsApiKey: gtaDestinationsApiKey,
		jwtKey: jwtKey,
		isFullAmount: isFullAmount,
		isLivePayment: isLivePayment,
		isBIN_CheckerEnabled: isBIN_CheckerEnabled,
		initiativeCodeName: initiativeCodeName,
		buildNumber: buildNumber,
		env: env,
		/* ========[ LANDING PAGE ]========
		Doc  : ...
		Type : POST
		*/
		transferSearchPriceRequest :`${uobtpAvalonTransferUrl}/json/reply/TransferSearchPriceRequest/`,
		
		airportListUrl:`${uobtpAvalonTransferUrl}/json/reply/TransferSearchAirportRequest/`,
		
		transferSearchAccomodation:`${uobtpAvalonTransferUrl}/json/reply/TransferSearchAccommodationRequest`,
		
		transferSearchStation:`${uobtpAvalonTransferUrl}/json/reply/TransferSearchStationRequest`,
		/* ========[ TRANSFER LIST PAGE ]========*/
		
		TransferSearchChargeConditionRequest:`${uobtpAvalonTransferUrl}/json/reply/TransferSearchChargeConditionRequest`,
		/* ========[ DETAIL PAGE ]========*/
		
		/* ========[ PAYMENT PAGE ]========*/
		
		/* ========[ OTHERS ]========*/
		verifyEnrollmentRequest: `${uobtpAvalonTransferUrl}/json/reply/PaymentVerifyEnrollmentRequest`,
		// ===[ END PAYMENT API ]===
		transferBookRequestWithPayment : `${uobtpAvalonTransferUrl}/json/reply/TransferBookRequest`,
		
	}
}();
