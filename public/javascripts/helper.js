// initialize default
var init = {
	modalTitleNotFound : "Oops Sorry!",
	//modal title when something when wrong
	
	errorToolTip:"Required", 
	//error tooltip when user doesnt fill the required data 
	
	modalDataRequiredNotFilled :"Please fill required input", 
	//if the user doesnt fill the required data and still press the button will show alert modal
	
	errorSelectToolTip : "Please select from suggestion", 
	//if user doesnt choose suggested destination
	contactService :`Please contact our office for assistance, call 6252 6822 or email to <a style="color:#0081CE;" href="mailto:UOBTP-Online@uobgroup.com?Subject=Transfer" target="_top">UOBTP-Online@uobgroup.com</a>.`, 
	
	modalWaitProses:"Please wait, we still processing your airports",
	//loading airport data
	
	modalNotSelectedAirports:"Please select another destination, we can't find any airport in your destination", 
	//modal when nothing airport selected but still using airport pick up for destination
	
	filterNotFound:"Sorry, there is no transfer based on your filter", 
	//if nothing to show when user filtering transfers list    
	
	formatMomentDatePicker : "DD MMM YYYY",
	
	formatMomentDatePickerAPI :"YYYY-MM-DD", 
	
	formatDatePicker :"dd mmm yyyy",//format date picker when sending to API
	
	formatDatePickerShowing :"MMMM, DD YYYY", //format showing date to front end overview
	
	// formatMomentTimePicker :'HH:mm (hh:mm A)',
	formatMomentTimePicker :'hh:mm A',
	
	// formatTimePicker :'H:i (hh:i A)',
	formatTimePicker :'hh:i A',
	
	minDateRange : 5,//date for return
	minDate : 4, //date for depart, or rent vehicle
	birthDateMax : 150,
	birthDateDefault : 14
};
init.modalMessageSomethingWrong = `Sorry, we have a problem while sending your data. `+init.contactService;
init.modalMessageHomeNotFound = `We do not have transfer service for the destination you are searching for. `+init.contactService;
//transfer not found on API, or zero result



/*end initialize default*/
function setSS(name,data){
	/*Save data to sessionStorage*/
	sessionStorage.setItem(name, data);	
}
function getSS(name){
	/*Get saved data from sessionStorage*/
	var data = sessionStorage.getItem(name);
	return data;
}
function removeSS(name){
	/*Remove saved data from sessionStorage*/
	sessionStorage.removeItem(name);
}
function clearSS(){
	/*Remove all saved data from sessionStorage*/
	sessionStorage.clear();
}
function openLoader(msgText){
	$("#msgText").html(msgText);
	$('.loader').css('display', 'block');
}
function closeLoader(){
	$('.loader').css('display', 'none');
}
function openModal(msgTitle,msgText,dismissible=false){
	//set dismissable to true when its not important message, so user can click on other area to close modal
	$("input").blur();
	$("button").blur();
	
	$("#modal-title").html(msgTitle);
	$("#modal-message").html(msgText);
	$('.modal').modal({ dismissible: dismissible });
	$("#modal").modal('open');
}
function closeModal(){
	$("#modal").modal('close');
}

function formatNumber(num,prefix,postfix){
	return `${prefix} ${Intl.NumberFormat().format(num)} ${postfix}`;
}
/* End of Custome Autocomplete Template */
$('.error-tag').attr("data-tooltip",init.errorToolTip);
$('.error-tag-select').attr("data-tooltip",init.errorSelectToolTip);

function reInit(){
	String.prototype.capitalize = function() {
		return this.charAt(0).toUpperCase() + this.slice(1);
	}
	$('.modal').modal();
	$('select').material_select();
	$(".button-collapse").sideNav();
	$('.button-collapse').sideNav({
		menuWidth: 300, // Default is 300
		edge: 'right', // Choose the horizontal origin
		closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
		draggable: true, // Choose whether you can drag to open on touch screens,
		onOpen: function(el) { }, // A function to be called when sideNav is opened
		onClose: function(el) {  }, // A function to be called when sideNav is closed
	});
	$('.tooltipped').tooltip({
		delay: 50,
		position:'top'
	});
	
	$('.collapsible').collapsible({
		accordion: false, // A setting that changes the collapsible behavior to expandable instead of the default accordion style
	});
	
	$(".collapsible-header").each((idx,item)=>{
		$(item).click(()=>{
			/*$(".collapsible-header").each((idx,item)=>{
				$(item).find(".material-icons").html("keyboard_arrow_up")
			})*/
			var collaps = $(item).parents("li").hasClass("active");
			if(collaps){
				$(item).find(".material-icons").html("keyboard_arrow_up");
			}else{
				$(item).find(".material-icons").html("keyboard_arrow_down");
			}
		});
	})
	$( '.timepicker' ).attr("placeholder",moment().format(init.formatMomentTimePicker));
	
	$( '.datepicker' ).pickadate({
		selectMonths: true,
		selectYears: 100,
		closeOnClear: true,
		min:true,
		max:365,
		today: '',
		clear: '',
		close: '',
		container: 'body',
		format: init.formatDatePicker,
		formatSubmit:'dd/mm/yyyy',
		showMonthsShort: true,
		onOpen: function() {
		},
		onSet: function(picker) {
			if(picker.select) {
				this.close();
			}
		},
		onClose: function() {
			$('.datepicker').blur();
			$('.picker').blur();
			$("html").css("overflow","hidden");
		}
	});
	
	$('.timepicker').pickatime({
		min:"",
		format: init.formatTimePicker,
		interval:10,
		onStart: function() {
			/*console.log('Hello there :)')*/
		},
		onRender: function() {
			/*console.log('Whoa.. rendered anew')*/
		},
		onOpen: function() {
			/* console.log('Opened up')*/
		},
		onClose: function() {
			/*console.log('Closed now')*/
		},
		onStop: function() {
			/*console.log('See ya.')*/
		},
		onSet: function(context) {
			// console.log('Just set stuff:', context);
		}
	});
	var $input = $(".datepicker").pickadate();
	var picker = $input.pickadate("picker");
	$input.each((idx,input)=>{
		var id = input.getAttribute('id');
		var picker = $("#"+id).pickadate("picker");
		picker.set('select', moment(), { format: init.formatDatePicker }, {muted: true});
	});
	
	$("button.prevent").click((e)=>{
		e.preventDefault();
	});
	
	$(".range").ionRangeSlider({
		type: "double",
		hide_min_max: true,
		hide_from_to:true,
		prettify_separator:",",
		min: 0,
		to: 100,
		step:1,
		grid: true,
		grid_num : 1,
	});
}
$(document).ready(function(){
	reInit();
	var ErrorMessage = $("#ErrorMessage").html()
	if(ErrorMessage!==""){
		openModal(init.modalTitleNotFound,`${ErrorMessage}. ${init.contactService}`)
	}
	
});