'use strict';

var express = require('express');
var request = require('request');
var api = require('../config/tocco-api');
var moment = require('moment');

var router = express.Router();

function getProtocol (req) {
	var proto = req.connection.encrypted ? 'https' : 'http';
	proto = req.headers['x-forwarded-proto'] || proto;
	return proto.split(/\s*,\s*/)[0];
}
function getFormatedDate(date) {return moment(date,"DD MMM YYYY").format('YYYY-MM-DD'); } ;
function middleware(req,res,next){
	next();
}
// GET home landing page
router.get('/',middleware, (req, res, next) => {
	res.render('homes/index', {
		
	});
});

module.exports = router;
