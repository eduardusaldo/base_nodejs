## UOB Travel Transfer (using GTA-Tocco API)

### NodeJS

```bash
$ git clone git@borg.garasilabs.org:tocco/transfer.git
$ cd transfer
```

Make sure your NodeJs is the latest so all function will works, 
use NVM (Node Version Manager) for easy and better Node management:

```bash
nvm list
nvm install 7
nvm alias default 7
```

Always checking if there any new packages on `package.json` and `bower.json` using:

```bash
$ npm install
$ bower install
```

### Run the server in Development mode

```bash
$ npm run dev
```

Press `ctrl-c` to terminate the server.

### Run the server in Staging mode

```bash
$ npm run staging
```

Press `ctrl-c` to terminate the server.

### Run the server in Production mode

```bash
$ npm run prod
```

Press `ctrl-c` to terminate the server.
